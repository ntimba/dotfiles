# **** dotfiles ****
if [[ -d "$HOME/.dotfiles" ]]; then
	DOTFILES_DIR="$HOME/.dotfiles"
else
	echo "Unable to find dotfiles"
	return
fi

# Make utilities available
export PATH="$DOTFILES_DIR/bin:$PATH"
for DOTFILE in `find ~/.dotfiles`
do
  [ -f “$DOTFILE” ] && source “$DOTFILE”
done

# source files
# inclusion des fichiers dans le .zprofile
DOTFILES_DIR="$HOME/.dotfiles"
for file in `ls -a "$DOTFILES_DIR/system"`
do
  if [ ! -d $file ] || [ -f $file ]; then
    source $DOTFILES_DIR/system/$file
  fi
done

# editeur par défaut
export EDITOR=nano
