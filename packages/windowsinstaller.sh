#!/bin/bash 

# tableau des liens de telechargement
packages=(
	"https://download.sublimetext.com/Sublime%20Text%20Build%203176%20x64%20Setup.exe"
    "https://desktop-release.notion-static.com/Notion%20Setup%200.2.0.exe"
	"https://dl.google.com/tag/s/appguid%3D%7B3C122445-AECE-4309-90B7-85A6AEF42AC0%7D%26iid%3D%7B9648D435-67BA-D2A7-54D2-1E0B5656BF03%7D%26ap%3Duploader%26appname%3DBackup%2520and%2520Sync%26needsadmin%3Dtrue/drive/installbackupandsync.exe" 
 	"https://builds.balsamiq.com/mockups-desktop/Balsamiq_Mockups_3.5.15.exe"
 	"https://admdownload.adobe.com/bin/live/readerdc_fr_xa_crd_install.exe"
	"https://download.cloud.lastpass.com/windows_installer/lastpass_x64.exe"
 	"https://airdownload.adobe.com/air/win/download/30.0/AdobeAIRInstaller.exe"
	"https://secure-appldnld.apple.com/itunes12/091-81934-20180529-DAFCAAD0-5F77-11E8-99A3-4F9A897FD268/iTunes64Setup.exe"
	)

NEW_DOWNLOAD_DIR=$(userprofile 'Downloads/dotfiles_packages') # le nouveau dossier à créer
mkdir "$NEW_DOWNLOAD_DIR" # création du dossier dotfiles_packages

# télécharger les fichiers
for lien in "${packages[@]}"
do
	wget -P $NEW_DOWNLOAD_DIR $lien
done

# installer les fichiers
ls $NEW_DOWNLOAD_DIR | while read line ; do
	cmd.exe /C Start "" /w "%USERPROFILE%\Downloads\dotfiles_packages\\$line" /S
done
