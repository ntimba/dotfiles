firstname=$(reader_dotfiles_config "firstname" ~/.dotfiles.config)
lastname=$(reader_dotfiles_config "lastname" ~/.dotfiles.config)
email=$(reader_dotfiles_config "email" ~/.dotfiles.config)
ide=$(reader_dotfiles_config "ide" ~/.dotfiles.config)
web=$(reader_dotfiles_config "web" ~/.dotfiles.config)

git config --global user.name "$firstname $lastname"
git config --global user.email "$email"
git config --global core.excludesfile ~/.gitignore_global
git config --global core.editor "$ide"
git config --global merge.tool diff
git config --global commit.template $HOME/.gitmessage.txt
# git config --global color.ui true # 

# créer un fichier .gitmessage.txt
if [[ ! -f ~/.gitmessage.txt ]]; then
	touch .gitmessage.txt 
fi 