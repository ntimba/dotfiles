#!/usr/bin/env bash
# -----------------------------------------------
create_ini_file () {
	# Utilisation : create_ini_file $dir $php_vers
	dir="${1}"
	php_vers="${2}"
	if [[ ! -f "/etc/php/${php_vers}/apache2/conf.d/00-ioncube.ini" ]]; then
		# sudo touch "/etc/php/${php_vers}/apache2/conf.d/00-ioncube.ini"
		touch $HOME/00-ioncube.ini
		echo "zend_extension = \"/usr/lib/php/${dir}/ioncube_loader_lin_${php_vers}.so\"" >> $HOME/00-ioncube.ini
		sudo mv $HOME/00-ioncube.ini "/etc/php/${php_vers}/apache2/conf.d/"
		# echo "zend_extension = \"/usr/lib/php/${dir}/ioncube_loader_lin_${php_vers}.so\"" | tee "/etc/php/${php_vers}/apache2/conf.d/00-ioncube.ini"
		# rendre le fichier exécutable
		sudo chmod 777 "/etc/php/${php_vers}/apache2/conf.d/00-ioncube.ini"
	else
		echo "Le fichier 00-ioncube.ini existe déjà"
	fi
}
repeat_as_long_negative () {
	# Utilisation : de la fonction : repeat_as_long_negative $pattern $text
	pattern="${1}" # format du nom de domaine : domain.ltd
	text="${2}"
	while [[ -z $variable ]] || [[ ! $variable =~ $pattern ]]; do
		read -p "${text}" variable
	done
	echo $variable
}

# ------------------------------------------------

if [[ -d $HOME/.ioncube_installation ]]; then
	rm -R $HOME/.ioncube_installation
fi 

mkdir $HOME/.ioncube_installation 


wget -P $HOME/.ioncube_installation http://downloads3.ioncube.com/loader_downloads/ioncube_loaders_lin_x86-64.tar.gz
tar -xzvf $HOME/.ioncube_installation/ioncube_loaders_lin_x86-64.tar.gz -C $HOME/.ioncube_installation

echo -e "\n"
echo "||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||"
php --version
echo "||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||"

echo -e "\n"
php_vers=$(repeat_as_long_negative '[0-9]{1}\.[0-9]{1}' "Quel est votre version de PHP ? (uniquement premier chiffre decimal ex: 7.0 ) : " )

# Deplacer le fichier ioncube_loader
ioncube_loader_downloaded="$HOME/.ioncube_installation/ioncube/ioncube_loader_lin_${php_vers}.so"
if [[ -f $ioncube_loader_downloaded ]]; then
	# Dossier pour les extensions
	pattern="[0-9]{8}"
	for dir in $(ls /usr/lib/php )
	do
		if [[ $dir =~ $pattern ]]; then

			if [[ ! -f "/usr/lib/php/${dir}/ioncube_loader_lin_${php_vers}.so" ]]; then
				sudo cp $ioncube_loader_downloaded "/usr/lib/php/${dir}/ioncube_loader_lin_${php_vers}.so"
				create_ini_file $dir $php_vers
			else
				# Si le ficier existe on crée le fichier ini
				echo "Le fichier : /usr/lib/php/${dir}/ioncube_loader_lin_${php_vers}.so existe"
				create_ini_file $dir $php_vers
			fi

		fi
	done 
fi

# redemarage d'apache2
sudo systemctl restart apache2