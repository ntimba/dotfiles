#!/usr/bin/env bash

if [[ $USER != 'root' ]]; then
	echo "Le script doit être exécuter en root"
else
	read -p "Entrer votre nom d'utilisateur : " username

	if [[ ! $(which sudo) ]]; then
		apt install sudo -y
	fi

	usermod -aG sudo $username

	if [[ ! $(grep "$username ALL=(ALL) ALL" /etc/sudoers) ]]; then
		echo "$username ALL=(ALL) ALL" >> /etc/sudoers
	fi

	systemctl restart ssh
fi 