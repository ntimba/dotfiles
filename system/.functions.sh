# La fonction va detecter le système
whichOS(){
  OS=$(uname -v)

  case $OS in
    $(grep -i 'darwin' <<< $OS) )
    # Installer des logiciels essential à mac de xcode
    echo 'macOS'
    ;;
    $(grep -i 'microsoft' <<< $OS) )
    echo 'WSL'
    ;;
    # Detection du sytème linux
    $(grep -i 'linux' <<< $OS) )
    echo 'linux'
    ;;
  esac
}

# ----------------------------------
whichSH(){
  echo $SHELL
}
# ----------------------------------
packageManagerINSTALL(){
  case $(whichOS) in
    macOS)
    # Installation de brew
    if [[ ! $( which brew ) ]];then
      /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
    fi
    ;;
    wsl)
    # Installation de choco
    echo "Installer le package manager choco : https://chocolatey.org/"
    ;;
  esac
}
# ----------------------------------
# Deinstallation des package manager
packageManagerUNINSTALL(){
  echo 'chancy'
  case $(whichOS) in
    'macOS')
      # Installation de brew
      if [[ ! $( which brew ) ]];then
        /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/uninstall.sh)"
      fi
    ;;
    'wsl')
    # Installation de choco
    echo "Installer le package manager choco : https://chocolatey.org/"
    ;;
  esac
}
# ----------------------------------

# cette fonction va créer des liens symbolique dans le repertoire $HOME
createSymLINK(){
  # Si le fichier n'existe pas dans le dossier
  ln -svf $1 "${HOME}/"
}
# ----------------------------------

unLinkProfile(){
  if [[ -L "${HOME}/.zprofile" ]]; then
    # créer un lien symbolique
    unlink "${HOME}/.zprofile"
  fi
}

# ----------------------------------
make_executable ()
{
	# rendre les fichier d'un dossier exécutable
	for file in `ls $1`
	do
		sudo chmod +x $1/$file
	done
}

# ----------------------------------
userHOME(){
  case $(whichOS) in
    macOS)
      echo $HOME
    ;;

    WSL)
      win_userprofile="$(cmd.exe /c "<nul set /p=%UserProfile%" 2>/dev/null)"

  		win_userprofile_drive="${win_userprofile%%:*}:"
  		userprofile_mount="$(findmnt --noheadings --first-only --output TARGET "$win_userprofile_drive")"

  		win_userprofile_dir="${win_userprofile#*:}"

  		userprofile="${userprofile_mount}${win_userprofile_dir//\\//}"

  		echo "/mnt/c$userprofile"
    ;;

    linux)
      echo $HOME
    ;;
  esac
}
# ---------------------------------
