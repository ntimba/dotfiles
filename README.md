Avant toute installation rassurer vous d'avoir git déjà installer

# Dotfiles
Voila mon dofiles

# Windows
Activer wsl2 source : https://bit.ly/3dnB5Rm
1. Ouvrer le PowerShell
2. Entrer la commande suivante :
```
dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart
```
# Mac
```
sudo softwareupdate -i -a
xcode-select --install
```
## Installer une distribution depuis le Store de microsoft
Ubuntu par exemple

# Installation Dotfiles
## Linux, macOS, wsl2
1. Installer git avec `sudo apt install git`
2. faire un git clone pour copier le dotfiles : `git clone https://gitlab.com/ntimba/dotfiles.git ~/.dotfiles`
3. Installer tout le contenu du dotfiles avec la commande
```
cd ~/.dotfiles
make
```
5. Redemarrer le système

# Les commandes dotfiles
`
Commands:
  dotfiles [ edit ] 		 pour ouvrir dotfiles dans un editeur de texte
	dotfiles [ pull ] 		 pour importer les fichiers dans le dotfiles
	dotfiles [ push ] 		 pour mettres à jour les fichier du systeme
	dotfiles [ clean ] 		 pour netoyer les caches des packages managers
	dotfiles [ update ] 		 pour mettre tout les logiciels à jour
	dotfiles [ config ] 		 pour modifier le fichier dotfiles.config
`

# Browser
- Chrome
- FireFox
- IE

# Editeur de Text
- PhpStorm
- Sublime Text 3

# Plugins Editeur
- Package Control
- livereload
- A File Icon
- Alignment
- AutoFileName
- DocBlockr
- Emmet
- Sass
- SublimeGit
- SublimeLinter

# Plugins Browser
- livereload
- Forest
- Pocket
- uBlock
- Thieve
- AliDropship
- LastPass
- Pinterest
- bitly

# Notes & To-Do's
- Notion.so

# sources
- http://bit.ly/2Mcar0M

# coming
- recovery snap applications
