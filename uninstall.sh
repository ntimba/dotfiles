#!/usr/bin/env sh
DOTFILES="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export DOTFILES

# 1. Inclure les fonctions
source "${DOTFILES}/system/.functions.sh"

# Desinstaller brew
packageManagerUNINSTALL

# Enlever un lien symbolique
unLinkProfile


# Supprimer les aliases
unlink "${HOME}/.aliases"


# Supprimer les liens symbolique de tout les fichiers
