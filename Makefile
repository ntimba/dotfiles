DOTFILES := $(dir $(realpath $(firstword $(MAKEFILE_LIST))))

# 1. Inclure les fonctions
include-functions:
	source $DOTFILES/system/.functions.sh
	
# 2. Création des variable $USERHOME
newhome:
	USERHOME=$(userHOME)

# 3. Installation gestionnaires de paquets *** PAGES ***
brew:
	curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install | ruby

# 4. Installation des pacquets
git: brew
	brew install git git-extras

brew-packages: brew
	brew bundle --file=$(DOTFILES)/packages/Brewfile

cask-apps: brew
	brew bundle --file=$(DOTFILES)/packages/Caskfile

node-packages:
		npm install -g $(shell cat packages/npmfile)

# 5. création link PROFILE
link-profile:
	ln -svf $(DOTFILES)/rc/.zprofile "${HOME}/"

unlink-runcom:
	unlink "${HOME}/.zprofile"

# 7. créer des liens symbolique ALIASES
link-alias:
	ln -svf $(DOTFILES)/system/.aliases "${HOME}/"

unlink-alias:
	unlink "${HOME}/.aliases"

# 8. prendre en compte .zprofile
include-profile:
	source ~/.zprofile

# 9. Installation des pacquets
packages: brew-packages cask-apps node-packages gems
