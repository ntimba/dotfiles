#!/usr/bin/env bash

# generer le ssh-keygen
read -p "Votre Mot de passe bitbucket : " -s password

echo -ne "\n\n\n" | ssh-keygen -t rsa
# modifier le mot de passe
ssh-keygen -p  -N "$password" -f ~/.ssh/id_rsa

echo "# Copier le texte qui commence par ssh-rsa" >> ~/id_rsa.pub_copy
cat ~/.ssh/id_rsa.pub >> ~/id_rsa.pub_copy

gedit ~/id_rsa.pub_copy 