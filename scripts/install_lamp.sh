#!/usr/bin/env bash

LAMP=(
	apache2
	mariadb-server
	mariadb-client
	php
	php-common
	php-mysql
	php-gd
	php-cli
)

# ***installation de apache *** 
sudo apt update && sudo apt upgrade

for line in "${packages[@]}"
do 	
    sudo apt install -y $line
done 
# Point de restauration
echo -ne "1\nMariaDB_installation\n" | recovery
#
sudo echo -ne "y\ny\ny\ny\ny\n" | mysql_secure_installation

# installation phpmyadmin
sudo apt install phpmyadmin