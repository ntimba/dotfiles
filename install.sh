#!/usr/bin/env bash

# installation minimal sans applications graphiques
# changer d'editeur de text par défaut, mètre GEDIT en avant
# fichiers recovery, enlever l'heure, et laisser la date uniquement
# amelioration de recovery, apache impossible de demarrer apache2 après la recovery

DOTFILES="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export DOTFILES

# include lib
source $DOTFILES/lib.sh

# Installation dotfiles
if [[ -f "$HOME/.profile" ]]; then
	if [[ ! -f $HOME/.profile.bak  ]]; then
		mv $HOME/.profile $HOME/.profile.bak
		ln -svf "$DOTFILES/runcom/.profile" $HOME/ 
	else
		echo -e "\t[x] Dotfiles est déjà installé"
	fi
fi

# source ~/.bashrc
source ~/.profile

# rendre tout les fichiers du dotfiles exécutable
make_executable $DOTFILES/bin
make_executable $DOTFILES/scripts

# installer les alias des commandes
ln -svf $DOTFILES/system/.bash_aliases ~/

# créer un fichier .gitmessage.txt
if [[ ! -f ~/.gitmessage.txt ]]; then
	touch ~/.gitmessage.txt
fi

# Configuraiton Git
if [[ -f $DOTFILES/git/.gitconfig ]]; then
	dotfiles_push $DOTFILES/git/.gitconfig ~/.gitconfig
else
	source $DOTFILES/scripts/configuration_git.sh
fi

# installation de git & bitbucket ou github
if [[ -f ~/.ssh/id_rsa || -f ~/.ssh/id_rsa ]]; then
	echo "Une clé ssh existe, pour modifier la clé lancer le scripts : .dotfiles/scripts/bitbucket.sh"
else
	source $DOTFILES/scripts/bitbucket.sh
fi 

# créer un point de restauration
echo -ne "1\nfresh_installed_system\n" | recovery