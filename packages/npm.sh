#!/bin/bash
packages=(
	sass
	npm-install
	@bitwarden/cli 
	)

# npm nécessite nodejs
sudo apt install nodejs
sudo apt install npm

for line in "${packages[@]}"
do
    npm install -g $line
done
