#!/usr/bin/env sh

# S'assurer qu'on utilise zsh
if [[ $SHELL = '/bin/zsh' ]]; then
	DOTFILES="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
	export DOTFILES

	# 1. Inclure les fonctions
	# source "${DOTFILES}/system/.functions.sh"

	# 2. Créer la variable $USERHOME
	#créer la variable $USERHOME -> Exporter la variable en variable d'environnement ???
	USERHOME=$(userHOME)

	# 3. Installer les gestionnaires de paquets
	# installer uniquement sur les system macOS
	packageManagerINSTALL

	# installation des fichiers RUN COMMANDS
	createSymLINK $DOTFILES/rc/.zprofile

	# liens symbolique des fichier system
	createSymLINK $DOTFILES/system/.aliases

	# Prendre en compte le fichier .profile


	# rendre tout les fichiers du dotfiles exécutable
	make_executable "${DOTFILES}/bin"
else
	echo "\n\tyou would need zsh to install this dotfile\n\n"
fi
